#!/bin/bash
set -euo pipefail

version="$(git rev-parse HEAD)"

if [ "$(git rev-parse --abbrev-ref HEAD)" != 'master' ]; then
    if [[ -z "${DEPLOY_DANGEROUSLY_NON_MASTER+x}" ]]; then
        echo "I will only deploy from the master branch! Set DEPLOY_DANGEROUSLY_NON_MASTER if you want to skip this check!"
        exit 1
    fi
fi

if ! git diff-index --quiet HEAD --; then
    if [[ -z "${DEPLOY_DANGEROUSLY_DIRTY+x}" ]]; then
        echo "I will only deploy commited changes! Set DEPLOY_DANGEROUSLY_DIRTY if you want to skip this check!"
        exit 1
    fi
    version="${version}-dirty-$(date '+%s')"
fi

set -x

./build.sh
tempd="$(mktemp -d)"
ssh_control_path="${tempd}/control.sock"
cleanup() {
    ssh -oControlMaster=no -oControlPath="${ssh_control_path}" "${DEPLOY_HOST}" -O exit || true
    rm -rf "${tempd}"
}
trap cleanup EXIT INT TERM

ssh -oControlMaster=yes -oControlPath="${ssh_control_path}" -Nf "${DEPLOY_USER}@${DEPLOY_HOST}"
ssh -oControlMaster=no -oControlPath="${ssh_control_path}" "${DEPLOY_HOST}" mkdir -p "/opt/applin/versions/${version}"
tar cjf - build nginx systemd | ssh -oControlMaster=no -oControlPath="${ssh_control_path}" "${DEPLOY_HOST}" tar xjf - -C "/opt/applin/versions/${version}"
ssh -oControlMaster=no -oControlPath="${ssh_control_path}" "${DEPLOY_HOST}" ln -sfn "/opt/applin/versions/${version}" /opt/applin/live
if [[ -z "${DEPLOY_WITHOUT_RESTART+x}" ]]; then
    ssh -oControlMaster=no -oControlPath="${ssh_control_path}" "${DEPLOY_HOST}" sudo /bin/systemctl daemon-reload
    ssh -oControlMaster=no -oControlPath="${ssh_control_path}" "${DEPLOY_HOST}" sudo /bin/systemctl restart applin
    ssh -oControlMaster=no -oControlPath="${ssh_control_path}" "${DEPLOY_HOST}" sudo /bin/systemctl reload nginx
fi
ssh -oControlMaster=no -oControlPath="${ssh_control_path}" "${DEPLOY_HOST}" find -L /opt/applin/versions -maxdepth 1 -mindepth 1 -not -samefile /opt/applin/live -exec rm -rf \{} +

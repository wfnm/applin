# applin

Applin is a Discord bot-cum-website that allows for banned users to appeal via a website, and have the appeals subsequently be directed to a channel on Discord.

## Setup

1. Create an application on https://discordapp.com/developers/applications and create a bot for it. Make sure that it is not a public bot and it has the server members privileged intent.

2. Set up a ReCAPTCHA v2 checkbox site at https://www.google.com/recaptcha/admin/create.

3. (optional) Create a holding server where users may be directed when they are banned. This allows them to be messaged as they will share a server with you. Make sure all channels in the server have `@everyone` permissions set to deny.

4. Configure the bot (see below).

5. Invite the bot to the managed server. It must have the ban members permission. `https://discordapp.com/oauth2/authorize?client_id=<your client id>&scope=bot&permissions=4`

6. (optional) Invite the bot to the holding server, if management of the holding server is required. It must have the kick members permission. `https://discordapp.com/oauth2/authorize?client_id=<your client id>&scope=bot&permissions=2`

## Configuration

Applin is set up via environment variables, all prefixed with `APPLIN_`.

### Required

These settings must be set for the bot to function.

 * `APPLIN_BASEURL`
    Base URL of the website with no trailing slash, e.g. `https://appeals-from-naughty-users.com`. **It must be served over HTTPS, or users will not be able to submit appeals!**

 * `APPLIN_DISCORDCLIENTID`
    The client ID of the application from https://discordapp.com/developers/applications.

 * `APPLIN_DISCORDCLIENTSECRET`
    The client secret of the application from https://discordapp.com/developers/applications.

 * `APPLIN_TOKEN`
    The token of the bot from https://discordapp.com/developers/applications.

 * `APPLIN_APPEALSCHANNELID`
   The ID of the channel to send appeals to.

 * `APPLIN_LISTENADDR`
   The listening address for the HTTP server.

 * `APPLIN_RECAPTCHASITEKEY`
   The site key for ReCAPTCHA.

 * `APPLIN_RECAPTCHASECRETKEY`
   The secret key for ReCAPTCHA.

 * `APPLIN_CSRFSECRET`
   Generate a random string and put it here: 64 bytes is recommended. Make sure it is completely random!

 * `APPLIN_SESSIONSECRET`
   Generate a random string and put it here: 64 bytes is recommended. Make sure it is completely random!

 * `APPLIN_MANAGEDGUILDID`
   The ID of the server Applin will be managing.

### May be Required

 * `APPLIN_HOLDINGSERVERINVITE`
   An invite to the holding server to be sent to banned users.

 * `APPLIN_HOLDINGGUILDID`
   The ID of the holding server.

 * `APPLIN_HOLDINGEXEMPTROLEID`
   The ID of the role that is exempt from being kicked from the holding server.

### Optional

 * `APPLIN_STATUS`
   Status message to use.

 * `APPLIN_SYNCBANSPERIOD`
   How often to read all bans from the managed Discord server. Applin will be immediately notified of bans as they occur but does not sync the reasons for them immediately.

 * `APPLIN_HOLDINGSWEEPPERIOD`
   How often to kick users from the holding Discord server. Applin will kick users immediately from the holding server who are not banned while it is online, but setting this will place an upper limit on how long users may remain in the holding server if Applin did not manage to kick them.

 * `APPLIN_HOLDINGPERIOD`
   How long Applin will allow a user to stay in holding before they are kicked.

 * `APPLIN_DEBUG`
   Disables security features. **Do not use this unless you are debugging.**

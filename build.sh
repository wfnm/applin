#!/bin/bash
set -euxo pipefail
mkdir -p build
go generate ./templates
go build -trimpath -o build .

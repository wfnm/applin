package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"wooloo.farm/applin/templates"

	"github.com/bwmarrin/discordgo"
	"github.com/gorilla/csrf"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/kelseyhightower/envconfig"
	"github.com/valyala/quicktemplate"
	"golang.org/x/net/context/ctxhttp"
	"golang.org/x/oauth2"
)

type Config struct {
	Status              string        `default:""`
	BaseURL             string        `default:""`
	DiscordClientID     string        `default:""`
	DiscordClientSecret string        `default:""`
	Token               string        `default:""`
	AppealsChannelID    string        `default:""`
	HoldingServerInvite string        `default:""`
	ListenAddr          string        `default:""`
	RecaptchaSiteKey    string        `default:""`
	RecaptchaSecretKey  string        `default:""`
	CSRFSecret          string        `default:""`
	SessionSecret       string        `default:""`
	ManagedGuildID      string        `default:""`
	HoldingGuildID      string        `default:""`
	SyncBansPeriod      time.Duration `default:"1h"`
	HoldingSweepPeriod  time.Duration `default:"1h"`
	MustBeInHolding     bool          `default:"false"`
	HoldingExemptRoleID string        `default:""`
	Debug               bool          `default:"false"`
	UseHoldingChannels  bool          `default:"false"`
}

type ban struct {
	loaded bool
	reason string
}

type App struct {
	session  *discordgo.Session
	lis      net.Listener
	sessions sessions.Store
	router   *mux.Router
	config   *Config

	guildName   string
	guildNameMu sync.RWMutex

	knownBans   map[string]ban
	knownBansMu sync.RWMutex

	heldUserChannels   map[string]string
	heldUserChannelsMu sync.RWMutex
}

func (a *App) setGuildName(guildName string) {
	a.guildNameMu.Lock()
	defer a.guildNameMu.Unlock()
	a.guildName = guildName
}

func (a *App) getGuildName() string {
	a.guildNameMu.RLock()
	defer a.guildNameMu.RUnlock()
	return a.guildName
}

func newApp(c *Config) (*App, error) {
	if c.Debug {
		log.Printf("WARNING: App is running in DEBUG mode! Config: %+v", c)
	}

	if c.SessionSecret == "" {
		return nil, errors.New("c.SessionSecret is not set!")
	}

	if c.CSRFSecret == "" {
		return nil, errors.New("c.CSRFSecret is not set!")
	}

	session, err := discordgo.New(c.Token)
	if err != nil {
		return nil, err
	}

	lis, err := net.Listen("tcp", c.ListenAddr)
	if err != nil {
		return nil, fmt.Errorf("failed to listen: %w", err)
	}

	log.Printf("Listening on %s", lis.Addr())

	sessionStore := sessions.NewCookieStore([]byte(c.SessionSecret))
	sessionStore.Options.Secure = !c.Debug
	sessionStore.Options.HttpOnly = true
	sessionStore.Options.SameSite = http.SameSiteLaxMode

	session.StateEnabled = false
	session.SyncEvents = true

	router := mux.NewRouter()

	a := &App{
		session:          session,
		lis:              lis,
		sessions:         sessionStore,
		heldUserChannels: map[string]string{},
		router:           router,
		config:           c,
	}
	if err := a.syncBans(); err != nil {
		return nil, err
	}

	go func() {
		ticker := time.NewTicker(c.SyncBansPeriod)
		for {
			<-ticker.C
			if err := a.syncBans(); err != nil {
				log.Printf("Failed to run period ban sync: %s", err)
			}
		}
	}()

	router.Handle("/", http.HandlerFunc(a.handleAppeal)).Methods("GET", "POST")
	router.Handle("/login", http.HandlerFunc(a.handleLogin)).Methods("GET")
	router.Handle("/logout", http.HandlerFunc(a.handleLogout)).Methods("POST")
	router.Handle("/_check", http.HandlerFunc(a.handleCheck)).Methods("GET")

	session.Identify.Intents = discordgo.MakeIntent(
		discordgo.IntentsGuilds |
			discordgo.IntentsGuildBans |
			discordgo.IntentsGuildMembers |
			discordgo.IntentsGuildPresences)

	session.AddHandler(a.ready)
	session.AddHandler(a.guildCreate)
	session.AddHandler(a.guildUpdate)
	session.AddHandler(a.guildDelete)
	session.AddHandler(a.guildMemberAdd)
	session.AddHandler(a.guildMemberRemove)
	session.AddHandler(a.guildBanAdd)
	session.AddHandler(a.guildBanRemove)

	return a, nil
}

func (a *App) checkHeldMember(member *discordgo.Member) (bool, error) {
	// Never sweep bots.
	if member.User.Bot {
		return true, nil
	}

	for _, role := range member.Roles {
		if role == a.config.HoldingExemptRoleID {
			return true, nil
		}
	}

	// Check if user is actually banned.
	isBanned := func() bool {
		a.knownBansMu.RLock()
		defer a.knownBansMu.RUnlock()
		_, ok := a.knownBans[member.User.ID]
		return ok
	}()
	if isBanned {
		return true, nil
	}

	if err := func() error {
		channel, err := a.session.UserChannelCreate(member.User.ID)
		if err != nil {
			return err
		}

		if _, err := a.session.ChannelMessageSend(channel.ID, fmt.Sprintf("You have been automatically kicked from the holding server for **%s** as you do not have a removal on record.", a.getGuildName())); err != nil {
			return err
		}

		return nil
	}(); err != nil {
		log.Printf("Failed to send to admonition to %s: %s", member.User.ID, err)
	}
	if err := a.session.GuildMemberDeleteWithReason(a.config.HoldingGuildID, member.User.ID, "AUTOMATED: No ban on record."); err != nil {
		return true, err
	}

	return false, nil
}

func (a *App) loadBans() (map[string]ban, error) {
	knownBans := map[string]ban{}

	var after string

	for {
		var list []*discordgo.GuildBan

		v := url.Values{}

		uri := discordgo.EndpointGuildBans(a.config.ManagedGuildID)
		if after != "" {
			v.Set("after", after)
		}

		if len(v) > 0 {
			uri += "?" + v.Encode()
		}

		body, err := a.session.RequestWithBucketID("GET", uri, nil, discordgo.EndpointGuildBans(a.config.ManagedGuildID))
		if err != nil {
			return nil, err
		}

		if err := json.Unmarshal(body, &list); err != nil {
			return nil, err
		}

		if len(list) == 0 {
			break
		}

		for _, b := range list {
			knownBans[b.User.ID] = ban{
				loaded: true,
				reason: b.Reason,
			}
		}

		after = list[len(list)-1].User.ID
	}

	return knownBans, nil
}

func (a *App) syncBans() error {
	a.knownBansMu.Lock()
	defer a.knownBansMu.Unlock()
	knownBans, err := a.loadBans()
	if err != nil {
		return err
	}
	a.knownBans = knownBans
	log.Printf("Synced %d bans.", len(a.knownBans))
	return err
}

func (a *App) isHeld(userID string) bool {
	a.heldUserChannelsMu.RLock()
	defer a.heldUserChannelsMu.RUnlock()
	_, ok := a.heldUserChannels[userID]
	return ok
}

var errShouldBeLoaded = errors.New("reason should have been loaded")

func (a *App) banReason(userID string) (string, bool, error) {
	// See if the ban exists.
	b, ok := func() (ban, bool) {
		a.knownBansMu.RLock()
		defer a.knownBansMu.RUnlock()
		b, ok := a.knownBans[userID]
		if !ok {
			return ban{}, false
		}
		return b, true
	}()

	if !ok {
		return "", false, nil
	}

	// If the ban is already loaded, we can return the reason immediately.
	if b.loaded {
		return b.reason, true, nil
	}

	// Otherwise, we need to eagerly load everyone's bans.
	log.Printf("Ban for %s found not but not loaded, loading all bans from server.", userID)

	a.knownBansMu.Lock()
	defer a.knownBansMu.Unlock()
	knownBans, err := a.loadBans()
	if err != nil {
		return "", false, err
	}
	a.knownBans = knownBans
	log.Printf("Synced %d bans.", len(a.knownBans))

	b, ok = a.knownBans[userID]
	if !ok {
		return "", false, nil
	}

	if !b.loaded {
		return "", true, errShouldBeLoaded
	}

	return b.reason, true, nil
}

func (a *App) ready(_ *discordgo.Session, event *discordgo.Ready) {
	go a.session.UpdateStatus(0, a.config.Status)
}

const holdingChannelPrefix = "hold-"

func (a *App) guildCreate(_ *discordgo.Session, event *discordgo.GuildCreate) {
	if event.Guild.ID == a.config.ManagedGuildID {
		a.setGuildName(event.Guild.Name)
		return
	}

	if event.Guild.ID != a.config.HoldingGuildID {
		return
	}

	a.heldUserChannelsMu.Lock()
	defer a.heldUserChannelsMu.Unlock()

	userIDsSet := make(map[string]bool, len(event.Guild.Members))

	for _, member := range event.Guild.Members {
		if member.User.Bot {
			continue
		}

		allowed, err := a.checkHeldMember(member)
		if err != nil {
			log.Printf("Failed processing %s: %s", member.User.ID, err)
		}

		if !allowed {
			log.Printf("%s is not allowed in holding guild.", member.User.ID)
			continue
		}

		if allowed {
			userIDsSet[member.User.ID] = true
		}
	}

	for _, channel := range event.Guild.Channels {
		if !strings.HasPrefix(channel.Name, holdingChannelPrefix) {
			continue
		}

		userID := channel.Name[len(holdingChannelPrefix):]
		if !userIDsSet[userID] {
			// Member is not known.
			if _, err := a.session.ChannelDelete(channel.ID); err != nil {
				log.Printf("Failed to delete channel for unknown member %s: %s", userID, err)
				continue
			}
		} else {
			// Member is known, channel exists.
			a.heldUserChannels[userID] = channel.ID
			delete(userIDsSet, userID)
		}
	}

	for userID := range userIDsSet {
		// Member is known, channel does not exist.
		channelID, err := a.createHoldingChannel(userID)
		if err != nil {
			log.Printf("Failed to create holding channel for member %s: %s", userID, err)
			channelID = ""
		}
		a.heldUserChannels[userID] = channelID
	}

	log.Printf("Synced %d held users.", len(a.heldUserChannels))
}

func (a *App) createHoldingChannel(userID string) (string, error) {
	if !a.config.UseHoldingChannels {
		return "", nil
	}

	channel, err := a.session.GuildChannelCreateComplex(a.config.HoldingGuildID, discordgo.GuildChannelCreateData{
		Name: holdingChannelPrefix + userID,
		PermissionOverwrites: []*discordgo.PermissionOverwrite{
			{
				ID:    userID,
				Type:  "member",
				Allow: discordgo.PermissionReadMessages | discordgo.PermissionReadMessageHistory,
			},
		},
	})
	if err != nil {
		return "", err
	}

	if _, err := a.session.ChannelMessageSend(channel.ID, fmt.Sprintf(`<@%s> This is the holding server for **%s**. You **must** remain in this server for your appeal to be processed. Make **sure** DMs are enabled for this server. If you leave this server, you **will** be automatically banned and you will **no longer be able to appeal**.`, userID, a.getGuildName())); err != nil {
		a.session.ChannelDelete(channel.ID)
		return "", err
	}

	return channel.ID, nil
}

func (a *App) guildUpdate(_ *discordgo.Session, event *discordgo.GuildUpdate) {
	if event.Guild.ID != a.config.ManagedGuildID {
		return
	}

	a.setGuildName(event.Guild.Name)
}

func (a *App) guildDelete(_ *discordgo.Session, event *discordgo.GuildDelete) {
	if event.Guild.ID != a.config.HoldingGuildID {
		return
	}

	a.heldUserChannelsMu.Lock()
	defer a.heldUserChannelsMu.Unlock()
	a.heldUserChannels = map[string]string{}
	log.Printf("Synced %d held users.", len(a.heldUserChannels))
}

func (a *App) guildMemberAdd(_ *discordgo.Session, event *discordgo.GuildMemberAdd) {
	if event.GuildID != a.config.HoldingGuildID {
		return
	}

	if event.Member.User.Bot {
		return
	}

	allowed, err := a.checkHeldMember(event.Member)
	if err != nil {
		log.Printf("Failed processing %s: %s", event.Member.User.ID, err)
	}

	if !allowed {
		log.Printf("%s is not allowed in holding guild.", event.Member.User.ID)
		return
	}

	// Create a special channel for them.
	func() {
		a.heldUserChannelsMu.Lock()
		defer a.heldUserChannelsMu.Unlock()

		var channelID string
		channelID, err := a.createHoldingChannel(event.Member.User.ID)
		if err != nil {
			channelID = ""
			log.Printf("Failed to create holding channel for %s: %s", event.Member.User.ID, err)
		}

		a.heldUserChannels[event.Member.User.ID] = channelID
		log.Printf("Synced %d held users.", len(a.heldUserChannels))
	}()
}

func (a *App) guildMemberRemove(_ *discordgo.Session, event *discordgo.GuildMemberRemove) {
	if event.GuildID != a.config.HoldingGuildID {
		return
	}

	if event.Member.User.Bot {
		return
	}

	channelID := func() string {
		a.heldUserChannelsMu.Lock()
		defer a.heldUserChannelsMu.Unlock()
		channelID := a.heldUserChannels[event.Member.User.ID]
		delete(a.heldUserChannels, event.Member.User.ID)
		log.Printf("Synced %d held users.", len(a.heldUserChannels))
		return channelID
	}()

	isBanned := func() bool {
		a.knownBansMu.RLock()
		defer a.knownBansMu.RUnlock()
		_, ok := a.knownBans[event.Member.User.ID]
		return ok
	}()

	if isBanned && channelID != "" {
		if _, err := a.session.ChannelDelete(channelID); err != nil {
			log.Printf("Failed to delete holding channel for %s: %s", event.Member.User.ID, err)
		}

		if err := a.session.GuildBanCreateWithReason(a.config.HoldingGuildID, event.User.ID, "AUTOMATED: Left holding server", 0); err != nil {
			log.Printf("Failed to auto-ban on leave for %s: %s", event.Member.User.ID, err)
		}

		log.Printf("Banned for leaving holding server: %s", event.Member.User.ID)
	}
}

func (a *App) guildBanAdd(_ *discordgo.Session, event *discordgo.GuildBanAdd) {
	if event.GuildID != a.config.ManagedGuildID {
		return
	}

	a.knownBansMu.Lock()
	defer a.knownBansMu.Unlock()
	a.knownBans[event.User.ID] = ban{loaded: false}
	log.Printf("Ban added (but not loaded): %s", event.User.ID)
}

func (a *App) guildBanRemove(_ *discordgo.Session, event *discordgo.GuildBanRemove) {
	if event.GuildID != a.config.ManagedGuildID {
		return
	}

	a.knownBansMu.Lock()
	defer a.knownBansMu.Unlock()
	delete(a.knownBans, event.User.ID)
	log.Printf("Ban removed: %s", event.User.ID)
}

var sessionName = "sess"

type identity struct {
	UserID      string `json:"i"`
	DisplayName string `json:"n"`
}

func verifyRecaptcha(ctx context.Context, secret string, response string) error {
	resp, err := ctxhttp.PostForm(ctx, http.DefaultClient, "https://www.google.com/recaptcha/api/siteverify", url.Values{"secret": {secret}, "response": {response}})
	if err != nil {
		return err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var gresp struct {
		Success    bool     `json:"success"`
		ErrorCodes []string `json:"error-codes"`
	}
	if err := json.Unmarshal(body, &gresp); err != nil {
		return err
	}

	if !gresp.Success {
		return fmt.Errorf("failed to verify ReCAPTCHA: %v", gresp.ErrorCodes)
	}

	return nil
}

func (a *App) setIdentity(r *http.Request, w http.ResponseWriter, id *identity) error {
	s, err := a.sessions.Get(r, sessionName)
	if err != nil {
		return err
	}

	rawID, err := json.Marshal(id)
	if err != nil {
		return err
	}

	s.Values["i"] = rawID

	return s.Save(r, w)
}

func (a *App) identify(r *http.Request) (*identity, error) {
	s, err := a.sessions.Get(r, sessionName)
	if err != nil {
		return nil, err
	}

	rawID := s.Values["i"]
	if rawID == nil {
		return nil, nil
	}

	id := &identity{}
	if err := json.Unmarshal(rawID.([]byte), id); err != nil {
		return nil, err
	}

	return id, nil
}

func (a *App) handleCheck(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")

	ok := func() bool {
		a.knownBansMu.RLock()
		defer a.knownBansMu.RUnlock()
		_, ok := a.knownBans[id]
		return ok
	}()

	code := http.StatusNotFound
	if ok {
		code = http.StatusNoContent
	}
	w.WriteHeader(code)
}

func (a *App) handleLogout(w http.ResponseWriter, r *http.Request) {
	if err := func() error {
		s, err := a.sessions.Get(r, sessionName)
		if err != nil {
			return err
		}
		delete(s.Values, "i")
		if err := s.Save(r, w); err != nil {
			return err
		}

		http.Redirect(w, r, "/login", http.StatusFound)
		return nil
	}(); err != nil {
		log.Printf("Error handling logout request: %s", err)
		fmt.Fprintf(w, "An error has occurred.")
	}
}

func (a *App) handleLogin(w http.ResponseWriter, r *http.Request) {
	if err := func() error {
		id, err := a.identify(r)
		if err != nil {
			return err
		}

		if id != nil {
			http.Redirect(w, r, "/", http.StatusFound)
			return nil
		}

		conf := &oauth2.Config{
			ClientID:     a.config.DiscordClientID,
			ClientSecret: a.config.DiscordClientSecret,
			Scopes:       []string{"identify"},
			RedirectURL:  a.config.BaseURL + "/login",
			Endpoint: oauth2.Endpoint{
				TokenURL: "https://discordapp.com/api/oauth2/token",
				AuthURL:  "https://discordapp.com/api/oauth2/authorize",
			},
		}

		code := r.URL.Query().Get("code")
		if code == "" {
			http.Redirect(w, r, conf.AuthCodeURL(""), http.StatusFound)
			return nil
		}

		token, err := conf.Exchange(r.Context(), code)
		if err != nil {
			return err
		}

		client := conf.Client(r.Context(), token)
		rawResp, err := client.Get("https://discordapp.com/api/users/@me")
		if err != nil {
			return err
		}
		body, err := ioutil.ReadAll(rawResp.Body)
		if err != nil {
			return err
		}

		var resp struct {
			ID            string
			Username      string
			Discriminator string
		}
		if err := json.Unmarshal(body, &resp); err != nil {
			return err
		}

		if err := a.setIdentity(r, w, &identity{
			UserID:      resp.ID,
			DisplayName: fmt.Sprintf("%s#%s", resp.Username, resp.Discriminator),
		}); err != nil {
			return err
		}

		http.Redirect(w, r, "/", http.StatusFound)
		return nil
	}(); err != nil {
		log.Printf("Error handling login request: %s", err)
		fmt.Fprintf(w, "An error has occurred.")
	}
}

func (a *App) handleAppeal(w http.ResponseWriter, r *http.Request) {
	var userID string
	var reason string
	w.Header().Set("Content-Type", "text/html")
	if err := func() error {
		id, err := a.identify(r)
		if err != nil {
			return err
		}

		if id == nil {
			http.Redirect(w, r, "/login", http.StatusFound)
			return nil
		}

		userID := id.UserID

		banReason, ok, err := a.banReason(userID)
		if err != nil {
			return err
		}
		if !ok {
			qw := quicktemplate.AcquireWriter(w)
			defer quicktemplate.ReleaseWriter(qw)
			templates.StreamNotBanned(qw, csrf.Token(r), a.getGuildName(), userID, id.DisplayName)
			return nil
		}

		if a.config.MustBeInHolding && !a.isHeld(userID) {
			qw := quicktemplate.AcquireWriter(w)
			defer quicktemplate.ReleaseWriter(qw)
			templates.StreamNotHeld(qw, csrf.Token(r), a.getGuildName(), a.config.HoldingServerInvite, userID, id.DisplayName)
			return nil
		}

		if r.Method != "POST" {
			qw := quicktemplate.AcquireWriter(w)
			defer quicktemplate.ReleaseWriter(qw)
			templates.StreamAppeal(qw, csrf.Token(r), a.getGuildName(), a.config.RecaptchaSiteKey, a.config.HoldingServerInvite, userID, id.DisplayName, banReason)
			return nil
		}

		if err := r.ParseForm(); err != nil {
			return err
		}

		// Verify ReCAPTCHA.
		if err := verifyRecaptcha(r.Context(), a.config.RecaptchaSecretKey, r.Form.Get("g-recaptcha-response")); err != nil {
			return err
		}

		reason = r.Form.Get("reason")
		if reasonRunes := []rune(reason); len(reasonRunes) > 1500 {
			reason = string(reasonRunes[:1500])
		}

		if _, err := a.session.ChannelMessageSend(a.config.AppealsChannelID, fmt.Sprintf("Appeal from <@%s> (**%s**, ID: %s) (ban reason: %s):\n```\n%s\n```", userID, id.DisplayName, userID, banReason, reason)); err != nil {
			return err
		}

		qw := quicktemplate.AcquireWriter(w)
		defer quicktemplate.ReleaseWriter(qw)
		templates.StreamAppealComplete(qw, csrf.Token(r), a.getGuildName(), userID, id.DisplayName)

		log.Printf("Successfully handled appeal request for %s: %s", userID, reason)
		return nil
	}(); err != nil {
		log.Printf("Error handling appeal request for %s: %s", userID, err)
		fmt.Fprintf(w, "An error has occurred.")
	}
}

func (a *App) run() error {
	if err := a.session.Open(); err != nil {
		return err
	}

	var handler http.Handler = a.router
	handler = handlers.ProxyHeaders(handler)
	handler = csrf.Protect(
		[]byte(a.config.CSRFSecret),
		csrf.Path("/"),
		csrf.FieldName("_csrf_token"),
		csrf.CookieName("csrf"),
		csrf.Secure(!a.config.Debug),
		csrf.SameSite(csrf.SameSiteLaxMode),
	)(handler)
	return (&http.Server{
		Handler: handler,
	}).Serve(a.lis)
}

func main() {
	var c Config
	if err := envconfig.Process("applin", &c); err != nil {
		log.Fatalf("Failed to parse config: %s", err)
	}

	app, err := newApp(&c)
	if err != nil {
		log.Fatalf("failed to create app: %s", err)
	}

	if err := app.run(); err != nil {
		log.Fatalf("failed to run app: %s", err)
	}
}
